# hadolint ignore=DL3006
ARG IMAGE_FROM_SHA
# hadolint ignore=DL3007
FROM jfxs/ci-toolkit:latest as ci-toolkit

# hadolint ignore=DL3006
FROM ${IMAGE_FROM_SHA}

ARG IMAGE_FROM_SHA
ARG RELEASE_VERSION
ARG BUILD_DATE
ARG VCS_REF="DEV"

ENV container docker

LABEL maintainer="FX Soubirou <soubirou@yahoo.fr>" \
    org.opencontainers.image.title="slack-notify" \
    org.opencontainers.image.description="A lightweight Docker image to easily publish Slack message" \
    org.opencontainers.image.authors="FX Soubirou <soubirou@yahoo.fr>" \
    org.opencontainers.image.licenses="GPL-3.0-or-later" \
    org.opencontainers.image.version="${RELEASE_VERSION}" \
    org.opencontainers.image.url="https://cloud.docker.com/repository/docker/jfxs/slack-notify" \
    org.opencontainers.image.source="https://gitlab.com/fxs/docker-slack-notify" \
    org.opencontainers.image.revision=${VCS_REF} \
    org.opencontainers.image.created=${BUILD_DATE}

COPY files /usr/local/bin/
COPY --from=ci-toolkit /usr/local/bin/get-local-versions.sh /usr/local/bin/get-local-versions.sh

# hadolint ignore=DL3018
RUN apk --no-cache add \
        ca-certificates \
        curl \
 && /usr/local/bin/get-local-versions.sh -f ${IMAGE_FROM_SHA} -s slack-notify.sh=${RELEASE_VERSION}

# hadolint ignore=DL3059
RUN addgroup nobody root && \
    mkdir -p /app && \
    chgrp -R 0 /app && \
    chmod -R g=u /etc/passwd /app

WORKDIR /app
