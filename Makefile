
DOCKER = docker

IMAGE_NODE       = node:lts-bullseye
IMAGE_SHELLCHECK = koalaman/shellcheck:stable
IMAGE_HADOLINT   = hadolint/hadolint:latest
IMAGE_FROM       = alpine:latest
IMAGE_BUILT      = slack-notify
IMAGE_RF         = jfxs/robot-framework
IMAGE_RF_DIND    = jfxs/robot-framework-dind


## nodejs modules management
## -------------------------
yarn-install: ## Install nodejs modules with yarn. Arguments: [pull=n]
yarn-install:
	@if [ -z ${pull} ]; then \
		$(DOCKER) pull ${IMAGE_NODE} ; \
	fi
	$(DOCKER) run -t --rm -v ${PWD}:/root -w /root ${IMAGE_NODE} yarn --cache-folder /root/.node_cache install

yarn-outdated: ## Check outdated npm packages. Arguments: [pull=n]
yarn-outdated:
	@if [ -z ${pull} ]; then \
		$(DOCKER) pull ${IMAGE_NODE} ; \
	fi
	$(DOCKER) run -t --rm -v ${PWD}:/root -w /root ${IMAGE_NODE} yarn --cache-folder /root/.node_cache outdated || true

yarn-upgrade: ## Upgrade packages. Arguments: [pull=n]
yarn-upgrade:
	@if [ -z ${pull} ]; then \
		$(DOCKER) pull ${IMAGE_NODE} ; \
	fi
	$(DOCKER) run -t --rm -v ${PWD}:/root -w /root ${IMAGE_NODE} yarn --cache-folder /root/.node_cache upgrade

clean-node-modules: ## Remove node_modules directory
clean-node-modules:
	rm -rf .node_cache
	rm -rf node_modules

.PHONY: yarn-install yarn-outdated yarn-upgrade clean-node-modules

## Docker
## ------
docker-build-push: ## Build docker image. Arguments: [arch=arm64] [vcs_ref=c780b3a] [tag=myTag]
docker-build-push: docker-rmi
	$(eval arch := $(shell if [ -z ${arch} ]; then echo "amd64"; else echo "${arch}"; fi))
	$(eval latest_sha := $(shell docker pull ${IMAGE_FROM} >/dev/null 2>&1 && docker inspect --format='{{index .RepoDigests 0}}' ${IMAGE_FROM}))
	@echo ${latest_sha}
	$(eval version := $(shell git fetch && git tag --sort=committerdate -l v* | tail -n1 | sed 's/^v\(.*\)$$/\1/'))
	@echo ${version}
	$(eval tag := $(shell if [ -z ${tag} ]; then echo "${IMAGE_BUILT}:latest"; else echo "${tag}"; fi))
	$(eval build_date := $(shell date -u +'%Y-%m-%dT%H:%M:%SZ'))
	$(MAKE) set-version version="${version}"
	if [ "${arch}" = "arm64" ]; then \
		docker buildx build --progress plain --no-cache --platform linux/arm64 --build-arg IMAGE_FROM_SHA="${latest_sha}" --build-arg RELEASE_VERSION=${version} --build-arg VCS_REF=${vcs_ref} --build-arg BUILD_DATE=${build_date} -t ${tag} --push . && \
		docker pull ${tag}; \
	else \
		$(DOCKER) build --no-cache --build-arg IMAGE_FROM_SHA=${latest_sha} --build-arg RELEASE_VERSION=${version} --build-arg VCS_REF=${vcs_ref} --build-arg BUILD_DATE=${build_date} -t ${tag} . && \
		docker push ${tag}; \
	fi

docker-rm: ## Remove all unused containers
docker-rm:
	$(DOCKER) container prune -f

docker-rmi: ## Remove all untagged images
docker-rmi: docker-rm
	$(DOCKER) image prune -f

PHONY: docker-build-push docker-rm docker-rmi

## Tests
## ------
checks: ## Run linter checks
checks:
	$(DOCKER) run -t --rm -v ${PWD}:/mnt ${IMAGE_SHELLCHECK} files/*.sh
	$(DOCKER) run -t --rm -v ${PWD}:/mnt ${IMAGE_HADOLINT} hadolint /mnt/Dockerfile

rf-tests-simple: ## Run robot framework simple tests
rf-tests-simple:
	mkdir -p reports
	@if [ -z ${container} ]; then \
		chmod 777 reports && \
		$(DOCKER) run -t --rm -v ${PWD}/tests:/tests:ro -v ${PWD}/files:/files:ro -v ${PWD}/reports:/reports ${IMAGE_RF} robot --include simple --outputdir /reports -v TESTS_DIR:/tests -v SHELL_DIR:/files RF && \
		chmod 755 reports ; \
	else \
		robot --include simple --outputdir reports tests/RF ; \
	fi

rf-tests-curl: ## Run robot framework simple tests. Arguments: hook=XXXX/YYYY/ZZZZ
rf-tests-curl:
	test -n "${hook}"  # Failed if Slack web hook not set
	mkdir -p reports
	@if [ -z ${container} ]; then \
		chmod 777 reports && \
		$(DOCKER) run -t --user root  --rm -v ${PWD}/tests:/tests:ro -v ${PWD}/files:/files:ro -v ${PWD}/reports:/reports ${IMAGE_RF} /bin/sh -c "apk --no-cache add ca-certificates curl && robot --include curl --outputdir /reports -v WEB_HOOK:${hook} -v TESTS_DIR:/tests -v SHELL_DIR:/files RF" && \
		chmod 755 reports ; \
	else \
		robot -v WEB_HOOK:${hook} --include curl --outputdir reports tests/RF ; \
	fi

test-in-image: ## Sanity tests to validate built image. Arguments: version=1.3.2 hook=XXXX/YYYY/ZZZZ
test-in-image:
	test -n "${version}"  # Failed if image not set
	test -n "${hook}"  # Failed if Slack web hook not set
	@slack-notify.sh -v | grep -q ${version} && echo "slack-notify.sh - test1: OK" || (echo "slack-notify.sh - test1: KO" && exit 1)
	@slack-notify.sh -w ${hook} -m "Message from slack-notify.sh test-in-image" -p "Plain-text message from slack-notify.sh test-in-image"  && echo "slack-notify.sh - test2: OK" || (echo "slack-notify.sh - test2: KO" && exit 1)

PHONY: checks rf-tests-simple rf-tests-curl test-in-image

## Admin
## -----
import-commit-key: ## Import key for sign commit
import-commit-key:
	mkdir -p .gnupg
	$(DOCKER) run -it --rm -v ${PWD}:/root -w /root ${IMAGE_NODE} gpg --import /root/${PRIVATE_KEY}
	chmod -R 700 .gnupg
	$(DOCKER) run -it --rm -v ${PWD}:/root -w /root ${IMAGE_NODE} gpg --list-secret-keys --keyid-format LONG
	@echo "Run command: git config user.signingkey XXXX"
	@echo "Run command: git config commit.gpgsign true"

commit: ## Commit with Commitizen command line. Arguments: [pull=n]
commit:
	@if [ -z ${pull} ]; then \
		$(DOCKER) pull ${IMAGE_NODE} ; \
	fi
	$(DOCKER) run -it --rm -v ${PWD}:/root -w /root ${IMAGE_NODE} /root/yarn-commit.sh

set-version: ## Set version in shell scripts. Arguments: version=2.8.1
set-version:
	test -n "${version}"  # Failed if version not set
	@for filename in files/*.sh; do \
		sed -i 's/__VERSION__/${version}/g' "$${filename}"; \
	done

.PHONY: import-commit-key commit set-version

.DEFAULT_GOAL := help
help:
	@grep -E '(^[a-zA-Z_-]+:.*?##.*$$)|(^##)' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[32m%-30s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[32m##/[33m/'
.PHONY: help
